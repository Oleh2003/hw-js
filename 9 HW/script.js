"use strict"

function getArray(arr, parent = document.body) {

      let elemList = document.createElement('ul');
      parent.prepend(elemList);


      arr.forEach((el) => {
            let elemLi = document.createElement('li');
            elemList.append(elemLi);
            elemLi.textContent = el;
      });

}
getArray(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);

