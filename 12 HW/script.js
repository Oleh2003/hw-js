"use strict"


const btnWrapper = document.querySelector('.btn-wrapper');

document.addEventListener('keydown', key);

function key(elem) {
  let elemForKey = findElement(elem.key.toUpperCase());
  if (!elemForKey) return;
  unpaintAllKey();
  paintKey(elemForKey);
}

function findElement(keyValue) {
  return [...btnWrapper.children].find(elem => elem.textContent.toUpperCase() === keyValue);
}

function paintKey(elemForKey) {
  elemForKey.style.backgroundColor = 'blue';
}

function unpaintAllKey() {
  [...btnWrapper.children].forEach((elem) => {
    elem.removeAttribute('style');
  });
}