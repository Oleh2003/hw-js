"use strict"

// Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// setTimeout дає змогу викликати функцію один раз через певний інтервал часу.
// setInterval дає змогу викликати функцію регулярно, повторюючи виклик через певний інтервал часу.

// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Це планує виконання функції якнайшвидше. Але  викликає його лише після завершення виконання поточного скрипту.
// Таким чином, функція планується до запуску “відразу після” поточного скрипту.

// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// Оскільки setInterval() виконує функцію періодично без зупинки він буде
// впливати на наступний код. Тому його відключають за допомогою clearInterval().

const listImg = document.querySelectorAll("img");
let count = 0;
let sliderWorks = false;

function slider() {
    listImg[count].className = "image-to-show";
  if (count = (count + 1) % listImg.length);
  listImg[count].classList.add("active");
}
let sliderShow = setInterval(slider, 3000, (sliderWorks = true));

const stopBtn = document.querySelector(".stopBtn");
stopBtn.addEventListener("click", pause);

function pause() {
  clearInterval(sliderShow);
  sliderWorks = false;
}

const goBtn = document.querySelector(".goBtn");
goBtn.addEventListener("click", next);

function next() {
  if (!sliderWorks) {
    sliderShow = setInterval(slider, 3000, (sliderWorks = true));
  }
}
