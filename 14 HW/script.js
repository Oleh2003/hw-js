"use strict"

document.addEventListener('DOMContentLoaded', load)

function load(){
    document.body.classList.toggle('dark' , localStorage.getItem('darkmode') === 'true')
}

function darkMode(){
    const body = document.body;
    const darkMode = localStorage.getItem('darkmode') === 'true';

    localStorage.setItem('darkmode', !darkMode)
    body.classList.toggle('dark' , !darkMode)
}


const input = document.querySelector('.theme')

input.addEventListener('click', darkMode);



