"use strict"

function filterBy(array, typeDate) {
   
    return array.filter(item => typeof item !== typeDate);
}

console.log(filterBy(['hello', 'world', 23, '23', null], "string"));