"use strict"
//Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let getAllParagraph = document.querySelectorAll('p');
console.log(getAllParagraph);

let backgroundColor = function (element, color) {
    element = document.querySelectorAll(element);
    for (let i = 0; i < element.length; i++) {
        element[i].style.backgroundColor = color;
    }
}

backgroundColor('p', '#ff0000')

//Знайти елемент із id="optionsList". Вивести у консоль. 
//Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let optionsListId = document.querySelector('#optionsList');
console.log(optionsListId);

let parentListId = optionsListId.closest('div');
console.log(parentListId);

for(let i = 0; i < optionsListId.children.length; i++){
    console.log(`${optionsListId.children[i].nodeName} , ${optionsListId.children[i].nodeType}`);
    // console.log(optionsListId.children[i].nodeName)
    // console.log(optionsListId.children[i].nodeType);
}

//Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

let newParagraph = document.createElement('p');
newParagraph.className = 'testParagraph';
newParagraph.innerHTML = 'This is a paragraph'
document.querySelector('.header-content-wrapper').appendChild(newParagraph);

//Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

let getElement = document.querySelector('.main-header');
console.log(getElement);

for( let newElem of getElement.children){
    newElem.classList.add('nav-item')
}

//Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

for(let name of document.querySelectorAll('.section-title')){
    name.remove('.section-title')
}









