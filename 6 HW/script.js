"use strict"

function createNewUser() {
    let firstName = prompt("What's you first name?");
    let lastName = prompt("What's you last name?");
    let birthday = prompt("When is your birthday? (dd.mm.yyyy)");
    
    const newUser = {
        firstName,
        lastName,
        birthday,

        getLogin() {
            return(firstName[0].toLowerCase() +  this.lastName.toLowerCase());
        },
        getAge() {
            let d = this.birthday.split('.');
            if (typeof d[2] !== "undefined") {
                date = d[2] + '.' + d[1] + '.' + d[0];
                return ((new Date().getTime() - new Date()) / (24 * 3600 * 365.25 * 1000)) | 0;
            };
        },

        getPassword() {
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6));
        },
    };
    return newUser;
};

const user = createNewUser();
console.log(user.getAge());
console.log(user.getPassword());
console.log(user);
